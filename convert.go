package main

import (
	"encoding/json"
	"errors"
	"net/http"
	"regexp"
	"strings"
)

var nestedFormRe = regexp.MustCompile(`(\w+)\[(\w+)\]`)

func ConvertForm(form map[string][]string, nested bool) (map[string]interface{}, error) {
	// convert www-form to json structure
	mpObject := make(map[string][]string)
	mpProperties := make(map[string]interface{})
	nestedProperties := make(map[string]map[string]string)
	for k, v := range form {
		if strings.HasSuffix(k, "[]") {
			k = strings.Replace(k, "[]", "", 1)
		}
		if m := nestedFormRe.MatchString(k); m {
			key := nestedFormRe.ReplaceAllString(k, "$1")
			skey := nestedFormRe.ReplaceAllString(k, "$2")
			if _, ok := nestedProperties[key]; !ok {
				nestedProperties[key] = make(map[string]string, 0)
			}
			nestedProperties[key][skey] = v[0]
		} else if k == "h" {
			mpObject["type"] = []string{"h-" + v[0]}
		} else if k == "access_token" || k == "action" || k == "url" || strings.HasPrefix(k, "mp-") || !nested { // I don't know if mp-* keys should be in properties or root
			mpObject[k] = v
		} else {
			mpProperties[k] = v
		}
	}
	// There probably is a better way to do this:
	mpObjectM, err := json.Marshal(mpObject)
	if err != nil {
		return nil, err
	}
	micropubObjectMap := make(map[string]interface{})
	json.Unmarshal(mpObjectM, &micropubObjectMap)
	micropubObjectMap["properties"] = mpProperties
	for k, v := range nestedProperties {
		s := make([]map[string]string, 0)
		micropubObjectMap["properties"].(map[string]interface{})[k] = append(s, v)
	}
	return micropubObjectMap, nil
}

func CleanUrl(url string) string {
	url = strings.Replace(url, "https://", "", 1)
	url = strings.Replace(url, "http://", "", 1)
	if strings.HasSuffix(url, "/") {
		url = url[:len(url)-1]
	}
	return url
}

func ExtractToken(r *http.Request, body []byte) (string, error) {
	if token := r.Header.Get("authorization"); len(token) > 0 {
		return strings.Replace(token, "Bearer ", "", 1), nil
	} else {
		cType, err := GetContentType(r.Header.Get("content-type"))
		if err != nil && r.Method != "GET" {
			return "", err
		}
		if cType == WwwForm || r.Method == "GET" {
			token = r.FormValue("access_token")
			if len(token) > 0 {
				return r.FormValue("access_token"), nil
			} else {
				return "", errors.New("no token provided")
			}
		}
		if cType == Json {
			var tokenstruct struct{ Access_token []string }
			err = json.Unmarshal(body, &tokenstruct)
			if err != nil {
				return "", err
			}
			if len(tokenstruct.Access_token) != 1 {
				return "", errors.New("no token provided")
			}
			return tokenstruct.Access_token[0], nil
		}
	}
	return "", errors.New("no token found")
}
