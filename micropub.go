package main

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
)

func HandleMicroPub(w http.ResponseWriter, r *http.Request) (int, error) {
	// check if the request is a POST
	if !(r.Method == "POST" || r.Method == "GET") {
		return 405, errors.New("only Post and Get Requests are Allowed")
	}
	// parse form before reading body
	r.ParseForm()
	//read the request body
	requestBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return 500, errors.New("Error reading request body")
	}

	// check content type
	contentType, err := GetContentType(r.Header.Get("content-type"))
	if err != nil && r.Method != "GET" {
		return 400, err
	}
	micropubObjectMap := make(map[string]interface{})
	if contentType == WwwForm || r.Method == "GET" {
		// convert form to a nested map
		micropubObjectMap, err = ConvertForm(r.Form, r.Method == "POST") // nested structure for post requests
		if err != nil {
			return 400, err
		}
	} else { // contentType == Json
		// unmarshal json to a map
		json.Unmarshal(requestBody, &micropubObjectMap)
		for k, v := range micropubObjectMap {
			switch vv := v.(type) {
			case string:
				micropubObjectMap[k] = []string{vv}
			}
		}
	}
	// add method field to distinguish between post and get requests
	micropubObjectMap["method"] = []string{r.Method}

	// remove token from map
	delete(micropubObjectMap, "access_token")
	// extract the request body from the request
	token, err := ExtractToken(r, requestBody)
	if err != nil {
		return 401, err
	}
	actioninterface, ok := micropubObjectMap["action"]
	var action string
	if !ok {
		action = "create"
		micropubObjectMap["action"] = []string{"create"}
	} else {
		switch a := actioninterface.(type) {
		case []string:
			action = a[0]
		case []interface{}:
			action = a[0].(string)
		case string:
			action = a
		case interface{}:
			action = a.(string)
		}
	}
	authorized, err := checkAccess(token, action)
	if err != nil {
		return 401, err
	}
	if authorized {
		// pass json to script
		micropubObjectJSON, _ := json.Marshal(micropubObjectMap)
		output, err := Submit(micropubObjectJSON)
		if err != nil {
			return 500, err
		}
		// set stdout as location header
		if r.Method == "POST" {
			if action == "delete" {
				w.WriteHeader(204)
				return 204, nil
			}
			w.Header().Add("Location", output)
			w.WriteHeader(201)
			return 201, nil
		} else if r.Method == "GET" {
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(200)
			w.Write([]byte(output))
			return 200, nil
		}
		return 201, nil
	}
	return 500, errors.New("Something went wrong")
}
