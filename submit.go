package main

import (
  "os/exec"
  "strings"
  "bytes"
  "errors"
)

func Submit(mpjson []byte) (string, error) {
  cmd := exec.Command(Script[0], Script[1:]...)
  var stdout bytes.Buffer
  var stderr bytes.Buffer
  cmd.Stdin = strings.NewReader(string(mpjson))
  cmd.Stdout = &stdout
  cmd.Stderr = &stderr
  err := cmd.Run()
  if err != nil {
    // I don't know how far this may be a security problem
    return "", errors.New(stderr.String())
  }
  return stdout.String(), nil
}
