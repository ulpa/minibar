## Usage

```
Usage:
  minibar site-url command [flags]

Flags:
  -h, --help                    help for micropub-server
  -p, --port string             The port micropub-server will bind to (default "5555")
  -t, --token-endpoint string   The Tokenendpoint you are using. (default "https://tokens.indieauth.com/auth")
```

where command gets called each publish. The command gets a json micropub object as stdin and should write the final url to stdout. If the exit code is not 0, micropub-server will include stderr in http body. For the exact behvior, check the [wiki](https://codeberg.org/ulpa/minibar/wiki)

#### Examples:
```micropub-server https://example.com/ ./myscript.sh -t https://token.example.com```  
```micropub-server example.org python3 myscript.py -p 8084``` 

## Installation

If you have go installed, use:
```go get codeberg.org/ulpa/minibar```

